%bcond_without check
%undefine _strict_symbol_defs_build
%global _configure_disable_silent_rules 1
%global bundled_rust_deps 0
%global cairo_version 1.16.0

Name:           librsvg2
Version:        2.57.92
Release:        3
Summary:        An SVG library based on cairo
License:        LGPLv2+
URL:            https://wiki.gnome.org/Projects/LibRsvg
Source0:        https://download.gnome.org/sources/librsvg/2.57/librsvg-%{version}.tar.xz
Source1:        vendor.tar.xz

Patch0:         0001-Modify-cargo-confign-file.patch

BuildRequires:  chrpath
BuildRequires:  gcc
BuildRequires:  gi-docgen
BuildRequires:  gobject-introspection-devel
BuildRequires:  make
BuildRequires:  pkgconfig(cairo) >= %{cairo_version}
BuildRequires:  pkgconfig(cairo-gobject) >= %{cairo_version}
BuildRequires:  pkgconfig(cairo-png) >= %{cairo_version}
BuildRequires:  pkgconfig(fontconfig)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gthread-2.0)
BuildRequires:  pkgconfig(libxml-2.0)
BuildRequires:  pkgconfig(pangocairo)
BuildRequires:  pkgconfig(pangoft2)
BuildRequires:  vala
BuildRequires:  python3-docutils
%if 0%{?bundled_rust_deps}
BuildRequires:  cargo
BuildRequires:  rust
%else
BuildRequires:  rust-packaging
%endif

Requires:       cairo >= %{cairo_version}
Requires:       cairo-gobject >= %{cairo_version}
Requires:       gdk-pixbuf2

%description
An SVG library based on cairo.

%package        devel
Summary:        Libraries and include files for developing with librsvg
Requires:       %{name} = %{version}-%{release}

%description    devel
This package provides the necessary development libraries and include
files to allow you to develop with librsvg.

%package tools
Summary:        Extra tools for librsvg
Requires:       %{name} = %{version}-%{release}

%description tools
This package provides extra utilities based on the librsvg library.

%package_help

%prep
%setup -q -n librsvg-%{version}
rm -vrf vendor .cargo Cargo.lock
tar xvf %{SOURCE1}
sed -i Makefile.am -e 's/$(CARGO) --locked/$(CARGO)/'
%cargo_prep
%patch0 -p1

%if 0%{?bundled_rust_deps}
%generate_buildrequires
%cargo_generate_buildrequires
%endif

%build
autoreconf -fiv
%configure --disable-static  \
           --disable-gtk-doc \
           --docdir=%{_datadir}/doc/%{name} \
           --enable-introspection \
           --enable-vala

%make_build

%install
%make_install
%delete_la

chrpath --delete %{buildroot}%{_bindir}/rsvg-convert
chrpath --delete %{buildroot}%{_libdir}/gdk-pixbuf-2.0/*/loaders/libpixbufloader-svg.so

rm -f %{buildroot}%{_pkgdocdir}/COMPILING.md

%files
%defattr(-,root,root)
%license COPYING.LIB
%{_libdir}/librsvg-2.so.*
%{_libdir}/gdk-pixbuf-2.0/*/loaders/libpixbufloader-svg.so
%{_libdir}/girepository-1.0/Rsvg-2.0.typelib
%{_datadir}/thumbnailers/*.thumbnailer

%files devel
%defattr(-,root,root)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/librsvg-2.0/
%{_datadir}/gir-1.0/Rsvg-2.0.gir
%{_datadir}/vala/vapi/librsvg-2.0.vapi
%{_datadir}/doc/%{name}/

%files tools
%defattr(-,root,root)
%{_bindir}/rsvg-convert

%files help
%defattr(-,root,root)
%doc %{_datadir}/doc/%{name}
%{_mandir}/man1/*.1*

%changelog
* Fri Jul 19 2024 liweigang <liweiganga@uniontech.com> - 2.57.92-3
- fix build error(automake 1.17)

* Fri Apr 19 2024 liweigang <liweiganga@uniontech.com> - 2.57.92-2
- modify vendor package format to tar.xz

* Wed Mar 13 2024 liweigang <liweiganga@uniontech.com> - 2.57.92-1
- update to version 2.57.92

* Wed Jan 31 2024 Paul Thomas <paulthomas100199@gmail.com> - 2.57.1-1
- update to version 2.57.1

* Wed Aug 30 2023 zhangpan <zhangpan103@h-partners.com> - 2.55.90-2
- fix CVE-2023-38633

* Thu Mar 23 2023 li-long315 <lilong@kylinos.cn> - 2.55.90-1
- Upgrade to 2.55.90

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 2.55.1-1
- Update to 2.55.1 and add cargo compile files

* Sat Jun 25 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 2.54.4-1
- Update to 2.54.4

* Wed Jun 8 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 2.54.3-1
- Update to 2.54.3

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 2.54.1-1
- Update to 2.54.1

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 2.54.0-1
- Update to 2.54.0

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.50.5-2
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git-core

* Wed May 19 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 2.50.5-1
- Upgrade to 2.50.5
- Update Version, Release

* Mon Apr 19 2021 zhanzhimin<zhanzhimin@huawei.com> - 2.50.2-2
- Type: bugfix
- ID:   NA
- SUG:  NA
- DESC: eliminate-difference

* Wed Jan 27 2021 hanhui <hanhui15@huawei.com> - 2.50.2-1
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC: update to 2.50.2

* Tue Nov 24 2020 zhanzhimin <zhanzhimin@huawei.com> - 2.50.1-2
- delete libcroco dependency

* Wed Nov 18 2020 zhanzhimin <zhanzhimin@huawei.com> - 2.50.1-1
- update to 2.50.1

* Thu Jul 23 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.46.4-1
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:update to 2.46.4

* Mon Jul 20 2020 yanan li <liyanan032@huawei.com> - 2.44.15-2
- fix build fail with rust 1.38.0

* Thu Jan 16 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.44.15-1
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:update to 2.44.15

* Wed Dec 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.44.6-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: optimization the spec of librsvg2

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.44.6-2
- Package init
